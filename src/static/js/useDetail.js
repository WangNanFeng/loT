$(function () {
  FastClick.attach(document.body);
  var params = urlParse();
  var userId = params.userId;
  var openId = params.openId;
  var partnerId = params.partnerId;
  var iccid = params.iccid;
  var method = 'queryUseDetailOfMonth';
  var useDetailWrapper = $('.use-detail-wrapper');
  var mask = $('.mask');
  var paramsData = {
    method: method,
    openId: openId,
    partnerId: partnerId,
    userId: userId,
    iccid: iccid
  }
  $.ajax({
    type: 'POST',
    url: url,
    data: paramsData,
    dataType: 'json',
    success: function (res) {
      if (res.resultCode === 0) {
        var result = JSON.parse(res.resultMsg);
        result.result.sort(function (a, b) {
          return b.datePoint.split('-')[2] - a.datePoint.split('-')[2];
        });
        var htmlStr = template('detail', result);
        useDetailWrapper.html(htmlStr);
        mask.hide();
      } else {
        prompt(res.resultMsg);
        mask.hide();
      }
    },
    error: function (xhr) {
      console.log(xhr);
    }
  });
  $('.back-button').on('click', function () {
    window.history.go(-1);
  });
});
