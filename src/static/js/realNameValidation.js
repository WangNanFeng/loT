$(function () {
  FastClick.attach(document.body);
  var ERR_OK = 0;
  var realName = $('.user-name');
  var certificateId = $('.id-number');
  var front = $('#front');
  var back = $('#back');
  var hold = $('#hold');
  var btn = $('.submit-btn');
  var photoOne = $('.photoOne');
  var photoTwo = $('.photoTwo');
  var photoThree = $('.photoThree');
  var dataObj = {};
  var params = urlParse();
  var userId = params.userId;
  var openId = params.openId;
  var partnerId = params.partnerId;
  var reg = /^[1-9]\d{5}(18|19|([23]\d))\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$/;
  var regName = /(^[\u4e00-\u9fa5]{1,}$)|(^[a-zA-z]{1,}(·?[a-zA-z]{1,})*$)/;
  var queryParams = {
    method: 'queryUseIsCertificate',
    openId: openId,
    partnerId: partnerId,
    userId: userId
  }
  // 查询
  $.ajax({
    type: 'post',
    url: url,
    data: queryParams,
    dataType: 'json',
    success: function (res) {
      if (res.resultCode === ERR_OK) {
        var result = JSON.parse(res.resultMsg);
        if (result.ischeck === ERR_OK) {
          render(result, '审核成功', '#67C23A');
        } else if (result.ischeck === 1) {
          render(result, '审核中', '#909399');
        } else if (result.ischeck === 2) {
          prompt('您的审核未通过，请您完善信息重新认证');
          btn.val('提交认证');
        }
      } else {
        prompt(res.resultMsg);
      }
    },
    error: function (xhr) {
      console.log(xhr);
    }
  });
  photoCompress(front, dataObj, 'photoOne', photoOne);
  photoCompress(back, dataObj, 'photoTwo', photoTwo);
  photoCompress(hold, dataObj, 'photoThree', photoThree);
  // 点击验证
  btn.on('click', function () {
    if (!validate()) {
      return;
    }
    btn.val('正在提交...').attr('disabled', 'disabled').css('background', '#ccc');
    var formData = new FormData();
    dataObj.realName = realName.val().trim();
    dataObj.certificateId = certificateId.val().trim();
    dataObj.openId = openId;
    dataObj.partnerId = partnerId;
    dataObj.userId = userId;
    for (var key in dataObj) {
      console.log(dataObj[key]);
      if (dataObj.hasOwnProperty(key)) {
        if (/photo[OT]{1,}/.test(key)) {
          formData.append(key, dataObj[key], key);
        } else {
          formData.append(key, dataObj[key]);
        }
      }
    }
    $.ajax({
      url: url + '?method=registerUserCertificateMsg',
      type: 'POST',
      processData: false, // 不处理数据
      contentType: false, // 不设置内容类型
      data: formData,
      dataType: 'json',
      success: function (res) {
        if (res.resultCode === ERR_OK) {
          prompt(res.resultMsg);
          $('label').hide();
          certificateId.attr('readonly', 'readonly');
          btn.val('审核中').attr('disabled', 'disabled').css('background', '#ccc');
        } else {
          prompt(res.resultMsg);
          btn.val('提交认证').removeAttr('disabled').css('background', '#f5a623');
        }
      },
      error: function (xhr) {
        console.log(xhr);
      }
    });
  });
  // 验证
  function validate() {
    if (!realName.val().trim()) {
      prompt('请输入用户姓名');
      return;
    }
    if (!regName.test(realName.val().trim())) {
      prompt('姓名不正确,请重新输入');
      return;
    }
    if (!certificateId.val().trim()) {
      prompt('请输入身份证号码');
      return;
    }
    if (!reg.test(certificateId.val().trim())) {
      prompt('请输入正确的身份证号码');
      return;
    }
    if (!front.val().length) {
      prompt('请选择上传身份证正面照');
      return;
    }
    if (!back.val().length) {
      prompt('请选择上传身份证反面照');
      return;
    }
    if (!hold.val().length) {
      prompt('请选择上传身份证手持正面照');
      return;
    }
    return true;
  }
  // 渲染
  function render(result, str, color) {
    $('label').hide();
    realName.val(result.realName);
    certificateId.val(result.certificateId);
    if (result.ischeck === 0) {
      photoOne.attr('src', result.photoOne);
      photoTwo.attr('src', result.photoTwo);
      photoThree.attr('src', result.photoThree);
    } else {
      photoOne.attr('src', result.photoOneTemp);
      photoTwo.attr('src', result.photoTwoTemp);
      photoThree.attr('src', result.photoThreeTemp);
    }
    if (result.ischeck === 0 || result.ischeck === 1) {
      realName.attr('readonly', 'readonly');
      certificateId.attr('readonly', 'readonly');
    }
    btn.val(str).attr('disabled', 'disabled').css('background', color);
  }

  // 压缩图片,预览
  function photoCompress(eleFile, dataObj, key, imgEle) {
    try {
      // 压缩图片需要的一些元素和对象
      var reader = new FileReader(),
        img = new Image();

      // 选择的文件对象
      var file = null;

      // 缩放图片需要的canvas
      var canvas = document.createElement('canvas');
      // 如果不支持,不压缩上传
      if (!canvas.toBlob) {
        throw new Error('浏览器不支持canvas.toBlob方法,请升级浏览器版本');
      }
      var context = canvas.getContext('2d');

      // base64地址图片加载完毕后
      img.onload = function () {
        // 图片原始尺寸
        var originWidth = this.width;
        var originHeight = this.height;
        // 最大尺寸限制
        var maxWidth = 400,
          maxHeight = 400;
        // 目标尺寸
        var targetWidth = originWidth,
          targetHeight = originHeight;
        // 图片尺寸超过400x400的限制
        if (originWidth > maxWidth || originHeight > maxHeight) {
          if (originWidth / originHeight > maxWidth / maxHeight) {
            // 更宽，按照宽度限定尺寸
            targetWidth = maxWidth;
            targetHeight = Math.round(maxWidth * (originHeight / originWidth));
          } else {
            targetHeight = maxHeight;
            targetWidth = Math.round(maxHeight * (originWidth / originHeight));
          }
        }

        // canvas对图片进行缩放
        canvas.width = targetWidth;
        canvas.height = targetHeight;
        // 清除画布
        context.clearRect(0, 0, targetWidth, targetHeight);
        // 图片压缩
        context.drawImage(img, 0, 0, targetWidth, targetHeight);
        // canvas转为blob并上传
        canvas.toBlob(function (blob) {
          dataObj[key] = blob;
        }, file.type || 'image/png');
      };

      // 文件base64化，以便获知图片原始尺寸
      reader.onload = function (e) {
        img.src = e.target.result;
        imgEle[0].src = e.target.result;
      };
      eleFile[0].addEventListener('change', function (event) {
        file = event.target.files[0];
        var isJPG = file.type === 'image/jpeg';
        var isPNG = file.type === 'image/png';
        var isLt5M = file.size / 1024 / 1024 < 5;
        if (!isJPG && !isPNG) {
          prompt('上传图片只能是 JPG/PNG 格式!');
          return;
        }
        if (!isLt5M) {
          prompt('上传图片大小不能超过 5MB!');
          return;
        }
        // 选择的文件是图片
        if (file.type.indexOf('image') === 0) {
          reader.readAsDataURL(file);
        } else {
          prompt('请上传正确的图片格式');
        }
      });
    } catch (error) {
      // 压缩图片需要的一些元素和对象
      var reader = new FileReader();
      // 文件base64化，以便获知图片原始尺寸
      reader.onload = function (e) {
        imgEle[0].src = e.target.result;
      };
      eleFile[0].addEventListener('change', function (event) {
        file = event.target.files[0];
        var isJPG = file.type === 'image/jpeg';
        var isPNG = file.type === 'image/png';
        var isLt5M = file.size / 1024 / 1024 < 5;
        if (!isJPG && !isPNG) {
          prompt('上传图片只能是 JPG/PNG 格式!');
          return;
        }
        if (!isLt5M) {
          prompt('上传图片大小不能超过 5MB!');
          return;
        }
        // 选择的文件是图片
        if (file.type.indexOf('image') === 0) {
          reader.readAsDataURL(file);
          dataObj[key] = file;
        } else {
          prompt('请上传正确的图片格式');
        }
      });
    }

  }
});
