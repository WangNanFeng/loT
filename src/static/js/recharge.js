$(function () {
  FastClick.attach(document.body);
  var params = urlParse();
  var userId = params.userId;
  var openId = params.openId;
  var partnerId = params.partnerId;
  var ERR_OK = 0;
  var method = 'getProductInfo';
  var paramsData = {
    method: method,
    userId: userId,
    openId: openId,
    partnerId: partnerId,
  }
  $('.submit-btn').on('click', function () {
    var iccid = $.trim($('.card-number').val());
    if (!iccid) {
      prompt('请输入ICCID卡号');
      return false;
    }
    if (iccid.length !== 20 && iccid.length !== 19) {
      prompt('ICCID卡号输入错误,请重新输入!');
      return false;
    }
    paramsData.iccid = iccid;
    var query = '?userId=' + userId + '&openId=' + openId + '&partnerId=' + partnerId + '&iccid=' + iccid;
    $.ajax({
      type: 'POST',
      url: url,
      data: paramsData,
      dataType: 'json',
      success: function (res) {
        if (res.resultCode === ERR_OK) {
          var result = JSON.parse(res.resultMsg);
          if (result.respCode === '00') {
            window.location.href = 'rechargeImmediately.html' + query;
          } else {
            prompt(result.respDesc);
          }
        } else {
          prompt(res.resultMsg)
        }
      },
      error: function (xhr) {
        console.log(xhr);
      }
    });
    return false;
  });
});
