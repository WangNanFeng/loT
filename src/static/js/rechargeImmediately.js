$(function () {
  FastClick.attach(document.body);
  var params = urlParse();
  var userId = params.userId;
  var openId = params.openId;
  var partnerId = params.partnerId;
  var iccid = params.iccid;
  var ERR_OK = 0;
  var method = 'getProductInfo';
  var paramsData = {
    method: method,
    userId: userId,
    openId: openId,
    partnerId: partnerId,
    iccid: iccid
  };
  var lis, customCode, customName, msisdn, payType = 2,
    businessType = 0;
  var mealWrapper = $('.meal-wrapper');
  var chioceWrapper = $('.choice-wrapper');
  var mask = $('.mask');
  // 定义过滤器
  template.defaults.imports.splitMoney = function (value) {
    return value.split('.')[0]
  };

  template.defaults.imports.packageSize = function (value) {
    return value < 1024 ? value + 'MB' : value / 1024 + 'GB';
  };

  template.defaults.imports.validityPeriod = function (value) {
    var packageType = value.split('_')[1].split('')[1];
    switch (packageType) {
      case 'm':
        return 1;
      case 'q':
        return 3;
      case 'h':
        return 6;
      case 'y':
        return 12;
      default:
        break;
    }
  };
  // 获取产品
  $.ajax({
    type: 'POST',
    url: url,
    async: false,
    data: paramsData,
    dataType: 'json',
    success: function (res) {
      if (res.resultCode === ERR_OK) {
        var result = JSON.parse(res.resultMsg);
        msisdn = result.msisdn;
        customCode = result.customCode;
        customName = result.customName;
        var list = result.pkgList;
        //运营商
        var yys = list[0].pkgCode.split('_')[0] === 'cmcc' ? '中国移动' : list[0].pkgCode.split('_')[0] === 'cucc' ? '中国联通' : '中国电信';
        // 设置用户信息
        setUserInfo(iccid, yys);
        var pakeages = fileterList(list);
        // 判断是否有叠加包
        var LEN = 0;
        for (var key in pakeages) {
          if (pakeages.hasOwnProperty(key)) {
            LEN += pakeages[key].length;
          }
        }
        if (LEN === 0) {
          mealWrapper.html('<p style="display: block;color: #000;font-size: .4rem;text-align: center;line-height: 1rem;">暂无可充值产品</p>');
          var chioceStr = '<li>您已选择：</li>' +
            '<li>有效期：</li>' +
            '<li>应付金额：</li>';
          chioceWrapper.html(chioceStr);
          $('.recharge-button').prop('disabled', 'disabled').css('background', '#909399');
          mask.hide();
          return false;
        }
        // 渲染产品
        renderProduct(pakeages);
        // 设置订单详情
        setOrderDetail();
        mask.hide();
      } else {
        prompt(res.resultMsg);
      }
    },
    error: function (xhr) {
      console.log(xhr);
    }
  });
  // 获取产品
  lis = $('.meal-wrapper .meal ul li');
  // 导航
  $('.nav').on('click', 'li', function () {
    var _this = $(this);
    var _index = _this.index();
    _this.addClass('active').siblings().removeClass('active');
    lis.removeClass('active');
    _this.parent().siblings('.meal-wrapper').find('li.meal').eq(_index).addClass('active').siblings().removeClass('active').end().find('li').eq(0).addClass('active');
    setOrderDetail();
  });

  // 产品
  lis.on('click', function () {
    var _this = $(this);
    lis.removeClass('active');
    _this.addClass('active');
    setOrderDetail();
  });
  // 立即充值
  $('.recharge-button').on('click', function () {
    var slectedProd = $('.meal-wrapper .meal ul li.active');
    var prod = slectedProd.data('prod');
    var fee = (slectedProd.data('price') / 1).toFixed(2);
    var packageType = slectedProd.data('validity');
    packageType = packageType === 1 ? 'm' : packageType === 3 ? 'q' : packageType === 6 ? 'h' : 'y';
    var body = '';
    var paramsData = {
      method: 'recharge',
      userId: userId,
      openId: openId,
      partnerId: partnerId,
      iccid: iccid,
      msisdn: msisdn,
      prod: prod,
      payType: payType,
      businessType: businessType,
      packageType: packageType,
      body: body,
      customCode: customCode,
      customName: customName,
      fee: fee
    };
    $.ajax({
      type: 'post',
      url: url,
      data: paramsData,
      dataType: 'json',
      success: function (res) {
        if (res.resultCode === 0) {
          if (res.resultMsg.status === 0) {
            pay(res.resultMsg);
          }
        } else if (res.resultCode === 55) {
          prompt('网络开小差,请稍后再试');
        } else {
          prompt(res.resultMsg);
        }
      },
      error: function (xhr) {
        console.log(xhr);
      }
    });
  });

  // 数组排序
  function sort(array) {
    return array.sort(function (a, b) {
      return a.pkgSize - b.pkgSize;
    });
  }

  // 过滤列表
  function fileterList(list) {
    var reg = /^c[mut]cc_o[mqhy]_\d{1,}M$/;
    var monthPackges = [],
      seasonPackges = [],
      halfPackges = [],
      fullPackges = [];
    list.map(function (item) {
      if (!reg.test(item.pkgCode)) {
        return;
      }
      var packageType = item.pkgCode.split('_')[1].split('')[1];
      switch (packageType) {
        case 'm':
          monthPackges.push(item);
          break;
        case 'q':
          seasonPackges.push(item);
          break;
        case 'h':
          halfPackges.push(item);
          break;
        case 'y':
          fullPackges.push(item);
          break;
        default:
          break;
      }
    });
    var pakeages = {
      monthPackges: sort(monthPackges),
      seasonPackges: sort(seasonPackges),
      halfPackges: sort(halfPackges),
      fullPackges: sort(fullPackges)
    };
    return pakeages;
  }

  // 渲染产品
  function renderProduct(pakeages) {
    // 渲染导航
    var navHtmlstr = '';
    for (var key in pakeages) {
      if (pakeages.hasOwnProperty(key)) {
        var p = pakeages[key];
        if (key === 'monthPackges' && p.length !== 0) {
          navHtmlstr += '<li>月套餐包</li>';
        } else if (key === 'seasonPackges' && p.length !== 0) {
          navHtmlstr += '<li>季套餐包</li>';
        } else if (key === 'halfPackges' && p.length !== 0) {
          navHtmlstr += '<li>半年套餐包</li>';
        } else if (key === 'fullPackges' && p.length !== 0) {
          navHtmlstr += '<li>年套餐包</li>';
        }
      }
    }
    $('.nav').html(navHtmlstr).find('li').eq(0).addClass('active');
    // 编译
    var htmlStr = template('packges', {
      pakeages: pakeages
    });
    mealWrapper.html(htmlStr).children().eq(0).addClass('active').find('ul').find('li').eq(0).addClass('active');
  }

  // 设置订单详情
  function setOrderDetail() {
    var activeLi = $('.meal .meal-item.active');
    var price = activeLi.data('price').split('.')[0];
    var size = activeLi.data('size');
    var validity = activeLi.data('validity');
    var chioceStr = '<li>您已选择：' + size + '</li>' +
      '<li>有效期：' + validity + '个自然月</li>' +
      '<li>应付金额：¥' + price + '</li>';
    chioceWrapper.html(chioceStr);
  }

  // 设置用户信息
  function setUserInfo(iccid, yys) {
    var userInfoStr = '<div class="user-number">用户号码：' + iccid + '</div>' +
      '<div class="yys">' + yys + '</div>';
    $('.user-info-wrapper').html(userInfoStr);
  }
});
