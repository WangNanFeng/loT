$(function () {
  FastClick.attach(document.body);
  var params = urlParse();
  var userId = params.userId;
  var openId = params.openId;
  var partnerId = params.partnerId;
  var iccid = params.iccid;
  var ERR_OK = 0;
  var method = 'queryInternetCardInfo';
  var baseInfoWrapper = $('.base-info-wrapper');
  var infoWrapper = $('.info-wrapper');
  var mask = $('.mask');
  var paramsData = {
    method: method,
    openId: openId,
    partnerId: partnerId,
    userId: userId,
    iccid: iccid
  }
  var query = '?userId=' + userId + '&openId=' + openId + '&partnerId=' + partnerId + '&iccid=' + iccid;
  // 定义过滤器
  template.defaults.imports.packageSize = function (value) {
    return value < 1024 ? value + 'MB' : value / 1024 + 'GB';
  }
  //查询
  $.ajax({
    type: 'POST',
    url: url,
    data: paramsData,
    dataType: 'json',
    success: function (res) {
      if (res.resultCode === ERR_OK) {
        var result = JSON.parse(res.resultMsg);
        var baseInfoStr = template('base-info', result);
        var infoStr = template('info-wrapper', result);
        baseInfoWrapper.html(baseInfoStr);
        infoWrapper.html(infoStr);
        mask.hide();
      } else {
        prompt(res.resultMsg);
        mask.hide();
      }
    },
    error: function (xhr) {
      console.log(xhr);
    }
  });

  // 导航
  $('.nav').on('click', 'li', function () {
    var _index = $(this).index();
    switch (_index) {
      case 0:
        window.location.href = 'useDetail.html' + query;
        break;
      case 1:
        window.location.href = 'rechargeImmediately.html' + query;
        break;
      case 2:
        window.location.href = 'orderQuery.html' + query;
        break;
      case 3:
        window.location.href = 'problem.html';
        break;
    }
  })
  return false;
});
