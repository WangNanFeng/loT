$(function () {
  var params = urlParse();
  var orderNo = params.orderNo;
  var createTime = params.createTime;
  var realCost = params.realCost;
  var recharge = params.recharge;
  var productName = params.productName;
  var payType = params.payType === '2' ? '微信支付' : '';
  var detailData = {
    orderNo: orderNo,
    createTime: createTime,
    realCost: realCost,
    recharge: recharge,
    payType: payType,
    productName: productName
  }
  var detailStr = template('detail', detailData);
  $('.detail-wrapper').html(detailStr);
  $('.back-button').on('click', function () {
    window.history.go(-1);
  });
});
