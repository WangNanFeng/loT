$(function () {
  FastClick.attach(document.body);
  var params = urlParse();
  var userId = params.userId;
  var openId = params.openId;
  var partnerId = params.partnerId;
  var ERR_OK = 0;
  var method = 'queryInternetCardInfo';
  var paramsData = {
    method: method,
    userId: userId,
    openId: openId,
    partnerId: partnerId,
  }
  $('.submit-btn').on('click', function () {
    var iccid = $.trim($('.card-number').val());
    if (!iccid) {
      prompt('请输入用户号码');
      return false;
    }
    paramsData.iccid = iccid;
    var query = '?userId=' + userId + '&openId=' + openId + '&partnerId=' + partnerId + '&iccid=' + iccid;
    $.ajax({
      type: 'POST',
      url: url,
      data: paramsData,
      dataType: 'json',
      success: function (res) {
        if (res.resultCode === ERR_OK) {
          window.location.href = 'queryResult.html' + query;
        } else {
          prompt(res.resultMsg);
        }
      },
      error: function (xhr) {
        console.log(xhr);
      }
    });
    return false;
  });
});
